import bcrypt from 'bcryptjs';

const data = {
  users: [
    {
      name: 'Admin',
      email: 'admin@example.com',
      password: bcrypt.hashSync('1234', 8),
      isAdmin: true,
    },
    {
      name: 'User',
      email: 'user@example.com',
      password: bcrypt.hashSync('1234', 8),
      isAdmin: false,
    },
  ],

 products:[
     {
    
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       
       image: '/images/p1.jpg',
       price: 120,
        countInStock: 10,
       brand: 'Nike',
       rating: 4.5,
       numReviews: 10,
       description: 'high quality product',

     },
      {
     
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       category: 'Shirts',
       image: '/images/p10.jpg',
       price: 100,
        countInStock: 20,
       brand: 'Adidas',
       rating: 4.0,
       numReviews: 14,
       description: 'high quality product',

     },
     {
     
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       category: 'Shirts',
       image: '/images/p3.jpg',
       price: 220,
        countInStock: 0,
       brand: 'Lacoste',
       rating: 4.8,
       numReviews: 17,
       description: 'high quality product',

     },
      {
     
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       category: 'Pants',
       image: '/images/p4.jpg',
       price: 78,
        countInStock: 15,
       brand: 'Nike',
       rating: 4.5,
       numReviews: 10,
       description: 'high quality product',

     },
      {
      
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       category: 'Pants',
       image: '/images/p5.jpg',
       price: 65,
        countInStock:5,
       brand: 'Puma',
       rating: 4.5,
       numReviews: 10,
       description: 'high quality product',

     },
      {
      
       name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
       category: 'Pants',
       image: '/images/p6.jpg',
       price: 139,
        countInStock: 7,
       brand: 'Adidas',
       rating: 4.5,
       numReviews: 15,
       description: 'high quality product',

     },
     {
      
      name: 'VL53L0X V2 TIME OF FLIGHT DISTANCE SENSOR (LIDAR)- 2000MM',
      category: 'Pants',
      image: '/images/p1.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p7.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p8.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p9.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p10.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p11.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p12.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p13.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p14.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p15.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     },
   {
      
      name: 'Adidas Fit Pants',
      category: 'Pants',
      image: '/images/p16.jpg',
      price: 139,
        countInStock: 7,
      brand: 'Adidas',
      rating: 4.5,
      numReviews: 15,
      description: 'high quality product',

     }
   
   
 ]
}

export default data;
