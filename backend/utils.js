import jwt from 'jsonwebtoken';

export const generateToken = (user) => {
  //by using sign we can generate a token
  return jwt.sign(
    {
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    },
    //read the jwt from .env file
    process.env.JWT_SECRET || 'somethingsecret',
    {
      expiresIn: '30d',
    }
  );
};