import {
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
  CART_SAVE_PAYMENT_METHOD,
  CART_SAVE_SHIPPING_ADDRESS,
} from '../constants/cartConstants';

export const cartReducer = (state = { cartItems: [] }, action) => {
  switch (action.type) {
    case CART_ADD_ITEM:
    // get the item to be added to the cart
      const item = action.payload;
    // to make sure that product is existing
      const existItem = state.cartItems.find((x) => x.product === item.product);
      if (existItem) {
        return {
          //to not change other properties
          ...state,
          //use map function to update product that already exists
          cartItems: state.cartItems.map((x) =>
            x.product === existItem.product ? item : x
          ),
        };
        //if the product is new and doesnt exist in cart items
      } else {
        // to add new item to a cart. Use ... to concatenate cart items with the items that come to this function, [...state.cartItems, item] -> concatenates cart items with the new items
        return { ...state, cartItems: [...state.cartItems, item] };
      }
      case CART_REMOVE_ITEM:
      return {
        //to not change cart properties 
        ...state,
        //remove items that contain action.payload because it contains ID
        //to only update properties of cart items
        cartItems: state.cartItems.filter((x) => x.product !== action.payload),
      };
        case CART_SAVE_SHIPPING_ADDRESS:
      return { ...state, shippingAddress: action.payload };
       case CART_SAVE_PAYMENT_METHOD:
      return { ...state, paymentMethod: action.payload };
    default:
      return state;
  }
};