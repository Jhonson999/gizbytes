const {PRODUCT_LIST_REQUEST, PRODUCT_LIST_SUCCESS, PRODUCT_LIST_FAIL, PRODUCT_DETAILS_REQUEST,
  PRODUCT_DETAILS_SUCCESS,
  PRODUCT_DETAILS_FAIL} = require('../constants/productConstants');
//after line 13 set default state to empty array to show products in homescreen by empty array to avoid null
export const productListReducer = (state = { loading: true, products: []}, action) => {
	// ec of action.type = PRODUCT_LIST_REQUEST, ETC...
	switch(action.type){
		case PRODUCT_LIST_REQUEST:
		//sending an AJAX to backend and wait for response
		return{loading: true};
		case PRODUCT_LIST_SUCCESS:
		//fetch products from the data that we got from backend
		return{loading: false, products: action.payload}
		case PRODUCT_LIST_FAIL:
		return{loading: false, error: action.payload}
	default:
	//don't change state at all
	return state;
		

	}
}

export const productDetailsReducer = (state ={ product:{}, loading: true}, action) =>{
	switch (action.type){
		case PRODUCT_DETAILS_REQUEST:
		return{loading: true};
		case PRODUCT_DETAILS_SUCCESS:
		return{loading: false, product: action.payload}
		case PRODUCT_DETAILS_FAIL:
		return{loading: false, error: action.payload}
		default:
		return state;

	}
}