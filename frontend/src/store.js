import {createStore, compose, applyMiddleware, combineReducers} from 'redux'
import { cartReducer } from './reducers/cartReducers';
import thunk from 'redux-thunk'

import{productListReducer, productDetailsReducer,} from './reducers/productReducers'
import {
  userRegisterReducer,
  userSigninReducer,
} from './reducers/userReducers';


// function that accepts 2 parameters
//returns lists of products from data.js in frontend
// object introduce reducers to redux store

const initialState = {
  //when refreshed will prevent from showing signin instead of username
  userSignin: {
    userInfo: localStorage.getItem('userInfo')
      ? JSON.parse(localStorage.getItem('userInfo'))
      : null,
  },

  cart: {
    // to read content of local storage
    cartItems: localStorage.getItem('cartItems')
    //JSON.parse to convert string to array
      ? JSON.parse(localStorage.getItem('cartItems'))
      : [],
      shippingAddress: localStorage.getItem('shippingAddress')
      ? JSON.parse(localStorage.getItem('shippingAddress'))
      : {},
      paymentMethod: 'PayPal',
  },
};
const reducer = combineReducers({
  productList: productListReducer,
  productDetails: productDetailsReducer,
  cart: cartReducer,
  userSignin: userSigninReducer,
   userRegister: userRegisterReducer,
})
// (state, action) => {
	//new state to be returned data.products
// 	return {products: data.products}
// }

//accepts reducer and initial state
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
	reducer, 
	initialState, 
	composeEnhancer(applyMiddleware(thunk))
);

export default store;